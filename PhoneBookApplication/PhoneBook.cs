﻿using System.Collections.Generic;

namespace PhoneBookApplication
{
    class PhoneBook
    {
        private List<Contact> list;
        public PhoneBook()
        {
            list = new List<Contact>();
        }

        public int Add(Contact contact)
        {
            if (IsValid(contact) && !Contains(contact))
            {
                list.Add(contact);
                return 1;
            }
            return 0;
        }

        public int AddRange(List<Contact> contacts)
        {
            int count = 0;
            foreach (var item in contacts)
            {
                count += Add(item);
            }
            return count;
        }

        public bool Contains(Contact contact)
        {
            foreach (var item in list)
            {
                if (item.PhoneNum == contact.PhoneNum)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsValid(Contact contact)
        {
            return contact.PhoneNum.Length == 9 && contact.PhoneNum[0] == '0';
        }

        public List<Contact> GetContactsByOperator(string code)
        {
            var listNums = new List<Contact>();
            foreach (var ct in list)
            {
                if (ct.PhoneNum.StartsWith(code))
                {
                    listNums.Add(ct);
                }
            }
            return listNums;
        }
    }
}
