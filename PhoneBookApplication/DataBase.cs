﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBookApplication
{
    static class DataBase
    {
        private static string[] codes = { "093", "044", "055", "091", "077", "099", "060" };

        public static List<Contact> GetContacts(int count)
        {
            var list = new List<Contact>(count);
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                int number6 = rnd.Next(100000, 999999);
                int randIndex = rnd.Next(codes.Length);
                string code = codes[randIndex];

                Contact ct = new Contact
                {
                    Name = $"A{i + 1}",
                    SurName = $"A{i + 1}yan",
                    PhoneNum = $"{code}{number6}"
                };
                list.Add(ct);
            }
            return list;
        }
    }
}
