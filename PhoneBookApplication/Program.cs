﻿using System;
using System.Collections.Generic;

namespace PhoneBookApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Contact> contacts = DataBase.GetContacts(10);
            PrintContacts(contacts);
            Console.WriteLine();

            var phoneBook = new PhoneBook();
            int count = phoneBook.AddRange(contacts);
            List<Contact> list = phoneBook.GetContactsByOperator("093");
            PrintContacts(list);

            Console.ReadLine();
        }

        static void PrintContacts(List<Contact> contacts)
        {
            foreach (var contact in contacts)
            {
                Console.WriteLine($"{contact.Name} {contact.SurName} { contact.PhoneNum}");
            }
        }

    }
}
